package tierce;

public class Cheval extends Thread implements Coureur {
	public String id;
	public int distance;
	Arbitre arb1;
	int position;
	
	public Cheval(String id,Arbitre arb1,int distance){
		this.distance = distance;
		this.id = id;
		this.arb1 = arb1;
		position = 0;
	}
	
	public int getPosition() {
		
		return position;
	}
	
	public void run(){
		while(position < distance){
			position++;
			try {
				Thread.sleep(50);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
